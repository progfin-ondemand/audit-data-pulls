
import json
from urllib import response
import requests
import base64
from requests.auth import HTTPBasicAuth
from json2xml import json2xml
from json2xml.utils import readfromurl, readfromstring, readfromjson
import pandas as pd
from pandas.io.json import json_normalize
import pprint
import datetime

####### need to get appPassword to run

#49 repos
repoList = [
"AutoFunding Service",
"Autofunding UI",
"DataGP",
"DataGP_Scribe",
"DataServices",
"DDE",
"Lease Acceptance Pipeline Manager",
"LeaseAdminProcessMgr",
"LeaseAdminService",
"LeaseTermsService",
"Merchant CRM",
"Operational Reports - SSRS",
"PaymentBounceServices",
"Prog",
"ProgLeasing.System.FeatureFlag",
"ProgLeasing.System.FeatureFlag.Contract",
"ProgLeasing.System.FeatureFlag.Demo",
"ProgLeasing.System.FeatureFlag.LaunchDarkly",
"ProgLeasing.System.FeatureFlag.LaunchDarkly.Framework",
"Progressive.BI.Reports",
"Progressive.BI.SOX",
"Progressive.Data",
"Progressive.Data.Integration",
"RevenueRecognition.Data",
"Stores Paid Pipeline Manager",
"StoresPaid Service",
"StoresPaid UI",
"StoresPaid_Scheduler",
"TaxMaintenanceService",
"TaxService",
"Stores Paid Micro Service",
"ccm-event-processor",
"ccm-event-receiver",
"ccm-migration-client",
"ccm-migration-lease",
"cservice-sch-dwstaging",
"cservice-sch-errorhandler",
"cservice-sch-merchant-crm",
"cservice-sch-mscrm",
"cservice-sys-dwstaging",
"cservice-sys-salesforce",
"edge-services-mscrm",
"store.locator.service",
"clam",
"clam.ui.hybrid",
"MyAccount.API",
"progressive.phoneverification",
"progressive.sso", 
"progressive.sso.manage" 
]

def postCreateRepoFromMaster(workSpace, repoName, userName, appPass, newBranch):
  response = requests.post(f'https://api.bitbucket.org/2.0/repositories/{workSpace}/{repoName}/refs/branches', auth=HTTPBasicAuth(userName, appPass), json={ 
	"name": newBranch, 
  "target": { "hash": "master" } 
})

# create repo 
def createRepo(workspaceId, repoSlug, userName, appPass):
  response = requests.post(f'https://api.bitbucket.org/2.0/repositories/{workspaceId}/{repoSlug}',  auth=HTTPBasicAuth(userName, appPass), headers={'Accept': 'application/json'})
  print(response.status_code)

def inviteReadPermission(repoName, userName, appPass, email):
    response = requests.post(f'https://api.bitbucket.org/1.0/invitations/dylandai/{repoName}', auth=HTTPBasicAuth(userName, appPass), data={
        'permission': 'read',
        'email': f'{email}'       
    })         

def inviteWritePermission(repoName, userName, appPass, email):
    response = requests.post(f'https://api.bitbucket.org/1.0/invitations/dylandai/{repoName}', auth=HTTPBasicAuth(userName, appPass), data={
        'permission': 'write',
        'email': f'{email}'       
    })         

def inviteDiffPermission(workspaceId, repoSlug, userName, appPass, accessLevel, email):
    response = requests.post(f'https://api.bitbucket.org/1.0/invitations/{workspaceId}/{repoSlug}', auth=HTTPBasicAuth(userName, appPass), data={
        'permission': f'{accessLevel}',
        'email': f'{email}'       
    })     
    
    print(response.status_code) 

def inviteDeletePermission(repoName, userName, appPass, email):
  response = requests.delete(f'https://api.bitbucket.org/1.0/invitations/dylandai/{repoName}', auth=HTTPBasicAuth(userName, appPass), data={
        'permission': 'read',
        'email': f'{email}'       
    })         

#workSpaceIdEmail, not needed here. Instead, using workspaceId, dylandai
def createNewGroup(workSpaceIdEmail, groupName, userName, appPass):
   response = requests.post(f'https://api.bitbucket.org/1.0/groups/{workSpaceIdEmail}/',  auth=HTTPBasicAuth(userName, appPass), data={
     'name': f'{groupName}'   
   })

   print(response.status_code)

def getGroupJson(workspaceId, userName, appPass):
   response_json = requests.get(f'https://api.bitbucket.org/1.0/groups/{workspaceId}/',  auth=HTTPBasicAuth(userName, appPass)).json()

   with open('C:\\automationTeam\\CMT\pythonOutputCsv\\groupMembers.json', 'w') as jsonFile:
    json.dump(response_json, jsonFile, indent=4)
    jsonFile.close()


def getGroupCsv(workspaceId, userName, appPass):
   response = requests.get(f'https://api.bitbucket.org/1.0/groups/{workspaceId}/',  auth=HTTPBasicAuth(userName, appPass))

   print(response.status_code)
   print(response.text)   
   
   pandaDataFrame = pd.json_normalize(response.json())
   pandaDataFrame.to_excel('C:\\automationTeam\\CMT\pythonOutputCsv\\groupMember.xlsx')

def addUserUuidtoGroup(workspaceId, groupSlug, userName, appPass, uuid):
  json_data = {}
  headers = {'Content-type': 'application/json'}

  response = requests.put(f'https://api.bitbucket.org/1.0/groups/{workspaceId}/{groupSlug}/members/{uuid}/', 
  auth=HTTPBasicAuth(userName, appPass), json=json_data, headers=headers)

  print(response.status_code)

def getGroupMember(workspaceId, groupSlug, userName, appPass):
  response = requests.get(f'https://api.bitbucket.org/1.0/groups/{workspaceId}/{groupSlug}/members/',  auth=HTTPBasicAuth(userName, appPass))

  print(response.status_code)
  print(response.text)

def grantGroupPrivilege(workspaceId, repoSlug, groupSlug, groupOwner, userName, appPass, privilege):
  reponse = requests.post(f'https://api.bitbucket.org/1.0/group-privileges/{workspaceId}/{repoSlug}/{groupOwner}/{groupSlug}', 
  auth=HTTPBasicAuth(userName, appPass), data=privilege)

  print(reponse.status_code)

# get single repo users permissions
def listRepoPermission(workspaceId, repoSlug, userName, appPass):
  headers = {'Accept': 'application/json'}
  response = requests.get(f'https://api.bitbucket.org/2.0/workspaces/{workspaceId}/permissions/repositories/{repoSlug}', auth=HTTPBasicAuth(userName, appPass), headers=headers)
  
  print(response.status_code)
  
  responseData = json.loads(response.text)  

  with open('C:\\automationTeam\\CMT\pythonOutputCsv\\listReposPermission.json', 'w') as jsonFile:
    json.dump(responseData, jsonFile, indent=4)
    jsonFile.close()

# only 49 repos, limited data, get groups permissions(write) on repoSlug
def getGroupPermissionForTheRepo49Limited(workspaceId, userName, appPass):
  #headers = {'Accept': 'application/json'}
  for i in range(0, len(repoList)):   
    #repoSlug = repoList[i]
    if repoList[i] == "Operational Reports - SSRS":
      repoSlug = "operational-reports-ssrs"     

    else: repoSlug = repoList[i]              

    url = (f'https://api.bitbucket.org/1.0/group-privileges/{workspaceId}/{repoSlug}/?filter=admin').replace(" ", "-")
    print(url)

    response = requests.get(url, auth=HTTPBasicAuth(userName, appPass))
    print(response.status_code)
    response2_json = requests.get(url, auth=HTTPBasicAuth(userName, appPass)).json()

    for j in range(0, len(response2_json)):
      for key in list(response2_json[j]['group']['owner']):
        #print(key)
        if key != 'display_name' and key != 'is_active':
          del response2_json[j]['group']['owner'][f'{key}']      
    
      for key in list(response2_json[j]['repository']['owner']):
        #print(key)
        if key != 'display_name' and key != 'is_active':
          del response2_json[j]['repository']['owner'][f'{key}']  

      for m in range(0, len(response2_json[j]['group']['members'])):
        for key in list(response2_json[j]['group']['members'][m]):
          #print(key)
          if key != 'display_name' and key != 'is_active':
            del response2_json[j]['group']['members'][m][f'{key}'] 
      
    with open('C:\\automationTeam\\CMT\pythonOutputCsv\\listGroupsPermissionForTheRepo49AdminLimited.json', 'a') as jsonFile:
      current_time = datetime.datetime.now()
      jsonFile.write("This info is pulled from BitBucket at: " + current_time.strftime('%Y%m%d_%H:%M:%S'))   
    
      json.dump(response2_json, jsonFile, indent=4)  
      
  jsonFile.close()

# 49 repos, get branch permissions
def getRepoRestrictions(workspaceId, repoSlug, userName, appPass):
  headers = {'Accept': 'application/json'}

  for i in range(0, len(repoList)):
        if repoList[i] == "Operational Reports - SSRS":
          repoSlug = "operational-reports-ssrs" 
        else: repoSlug = repoList[i]  

        url = (f'https://api.bitbucket.org/2.0/repositories/{workspaceId}/{repoSlug}/branch-restrictions?pagelen=20').replace(" ", "-")
        print(url)

        response = requests.get(url, auth=HTTPBasicAuth(userName, appPass), headers=headers)
        print(response.status_code)

        response_json = requests.get(url, auth=HTTPBasicAuth(userName, appPass), headers=headers).json()

        for j in range(0, len(response_json['values'])):
          
          for key in list(response_json['values'][j]):
            if key != 'kind' and key != 'users' and key != 'pattern' and key != 'groups' and key != 'value':
              del response_json['values'][j][f'{key}']

          for m in range(0, len(response_json['values'][j]['groups'])):
            for key in list(response_json['values'][j]['groups'][m]):
              if key != 'default_permission' and key != 'name' and key != 'type':
                del response_json['values'][j]['groups'][m][f'{key}']

          for n in range(0, len(response_json['values'][j]['users'])):
            for key in list(response_json['values'][j]['users'][n]):
              if key != 'display_name' and key != 'type':
                del response_json['values'][j]['users'][n][f'{key}']
      
        with open('C:\\automationTeam\\CMT\pythonOutputCsv\\branchPermission49Repos.json', 'a') as jsonFile:    
          current_time = datetime.datetime.now()
          jsonFile.write(" \n")
          jsonFile.write("This info is pulled from BitBucket at: " + current_time.strftime('%Y%m%d_%H:%M:%S\n'))   
          jsonFile.write(repoList[i] + ": Branch permissions\n") 

          json.dump(response_json, jsonFile, indent=4) 
        jsonFile.close()   

# this one currently not working due to a bug in their API
def modifyMasterToMain(workspaceId, repoSlug, userName, appPass):
  response = requests.put(f'https://api.bitbucket.org/2.0/repositories/{workspaceId}/{repoSlug}', auth=HTTPBasicAuth(userName, appPass), data={

    "mainbranch": {
      "type": "branch",
      "name": "main"
    }
  })
  print(response.status_code)
  print(response.text)

def listBranchRestrictions(workspaceId, repoSlug, userName, appPass):
  response = requests.get(f'https://api.bitbucket.org/2.0/repositories/{workspaceId}/{repoSlug}/branch-restrictions', auth=HTTPBasicAuth(userName, appPass))
  print(response.status_code)
  print(response.text)

def listDefaultReviewers(workspaceId, repoSlug, userName, appPass):
  response = requests.get(f'https://api.bitbucket.org/2.0/repositories/{workspaceId}/{repoSlug}/default-reviewers', auth=HTTPBasicAuth(userName, appPass))
  print(response.status_code)
  print(response.text)

def removeDefaultReviewer(workspaceId, repoSlug, uuid, userName, appPass):
  response = requests.delete(f'https://api.bitbucket.org/2.0/repositories/{workspaceId}/{repoSlug}/default-reviewers/{uuid}', auth=HTTPBasicAuth(userName, appPass))
  print(response.status_code)
  print(response.text)  

def addDefaultReviewer(workspaceId, repoSlug, uuid, userName, appPass):
  response = requests.put(f'https://api.bitbucket.org/2.0/repositories/{workspaceId}/{repoSlug}/default-reviewers/{uuid}', auth=HTTPBasicAuth(userName, appPass))
  print(response.status_code)
  print(response.text)   

def listWebhook(workspaceId, repoSlug, userName, appPass):
  response = requests.get(f'https://api.bitbucket.org/2.0/repositories/{workspaceId}/{repoSlug}/hooks', auth=HTTPBasicAuth(userName, appPass))
  print(response.status_code)
  print(response.text) 
  
  
# the following is for shuffle artifactory packages in AWS instance
# in order to run, you will need your own userName and apiKey, generated by artifactory

import json
import requests

baseUrl = 'https://art.api.progleasing.com/artifactory/api'
userName = ''
apiKey = ''

#find package(usauly located in, nuget-aws-release). 1st, move to different repo, like nuget-aws-prerelease. 2nd copy same package back to, nuget-aws-release
def shuffle(srcRepoKey, packageName, targetRepoKey):
    # move item
    response = requests.post(f'{baseUrl}/move/{srcRepoKey}/{packageName}?to=/{targetRepoKey}/{packageName}', auth=(userName, apiKey))
    print(response.status_code)
    print(response.text)

    # move/copy back
    response = requests.post(f'{baseUrl}/move/{targetRepoKey}/{packageName}?to=/{srcRepoKey}/{packageName}', auth=(userName, apiKey))
    print(response.status_code)
    print(response.text)


shuffle('nuget-aws-release', 'ProgLeasing.Platform.DepartmentApi.ServiceContract.1.0.0.nupkg', 'nuget-aws-prerelease')
  
  
  